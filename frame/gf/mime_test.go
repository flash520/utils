/**
 * @Author: koulei
 * @Description:
 * @File: mime_test.go
 * @Version: 1.0.0
 * @Date: 2022/4/9 14:37
 */

package gf

import (
	"fmt"
	"mime"
	"net/http"
	"os"
	"testing"
)

func TestMime(t *testing.T) {
	if err := Mime.InitMime(); err != nil {
		t.Error(err.Error())
	}

	var exts = []string{
		".m3u8",
		".ts",
		".flv",
		".mp4",
		".mov",
		".rpm",
		".zip",
		".7z",
		".rar",
		".ppt",
		".pptx",
		".doc",
		".docx",
		".xls",
		".xlsx",
	}
	for i, ext := range exts {
		fmt.Printf("%d: %s -> %s\n", i, ext, mime.TypeByExtension(ext))
	}

	file, err := os.Open("/Users/koulei/Downloads/playVideo.ts")
	if err != nil {
		panic(err.Error())
	}
	var buff = make([]byte, 512)
	_, err = file.Read(buff)
	if err != nil {
		panic(err.Error())
	}
	contentType := http.DetectContentType(buff)
	fmt.Println(contentType)
}
