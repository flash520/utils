/**
 * @Author: koulei
 * @Description: TODO
 * @File:  minio
 * @Version: 1.0.0
 * @Date: 2021/5/12 18:27
 */

package minio

import (
	"net"
	"net/http"
	"sync"
	"time"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	log "github.com/sirupsen/logrus"
)

var (
	transport = func(maxConnections int) *http.Transport {
		tr := &http.Transport{
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 90 * time.Second,
			}).DialContext,
			MaxIdleConns:          maxConnections,
			MaxIdleConnsPerHost:   maxConnections,
			MaxConnsPerHost:       maxConnections,
			IdleConnTimeout:       90 * time.Second,
			ResponseHeaderTimeout: 15 * time.Second,
			DisableKeepAlives:     false,
			DisableCompression:    true,
		}
		return tr
	}

	client *minio.Client
	err    error
)

type Minio struct {
}

func CreateMinio(addr, accessKey, secretKey string, maxConnections ...int) *Minio {
	var once sync.Once
	var conn int
	switch len(maxConnections) {
	case 1:
		conn = maxConnections[0]
	default:
		conn = 8
	}
	once.Do(func() {
		client, err = minio.New(addr, &minio.Options{
			Creds:     credentials.NewStaticV4(accessKey, secretKey, ""),
			Secure:    false,
			Transport: transport(conn),
		})
		if err != nil {
			log.Println(err.Error())
		}
	})

	return &Minio{}
}

func (m *Minio) GetConn() *minio.Client {
	return client
}
