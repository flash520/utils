/**
 * @Author: koulei
 * @Description:
 * @File: bloomfilter
 * @Version: 1.0.0
 * @Date: 2022/3/4 18:31
 */

package bloomfilter

import "math"

type BloomFilter interface {
	Set([]byte)
	Check([]byte) bool
}
type MemoryBloomFilter struct {
	MLength   int64   // 过滤器长度
	MArr      []int64 // 过滤器数组
	NCount    int64   // 插入元素个数
	FalseRate float64 // 误报率
	KHash     int     // hash函数个数
}

func NewBloomFilter(size int64, falseRate float64) BloomFilter {
	k, m := getFilterParams(size, falseRate)

	return &MemoryBloomFilter{
		MLength:   m,
		MArr:      make([]int64, m),
		NCount:    size,
		FalseRate: falseRate,
		KHash:     k,
	}
}

func getFilterParams(size int64, falseRate float64) (int, int64) {
	m := -1 * math.Log(falseRate) * float64(size) / (math.Ln2 * math.Ln2)
	k := m * math.Ln2 / float64(size)
	return int(k), int64(m)
}

func (bf *MemoryBloomFilter) Set(data []byte) {
	for i := 0; i < bf.KHash; i++ {
		index := bf.hashFun(data, i)
		bf.MArr[index] = 1
	}
}

func (bf *MemoryBloomFilter) Check(data []byte) bool {
	for i := 0; i < bf.KHash; i++ {
		index := bf.hashFun(data, i)
		if bf.MArr[index] == 0 {
			return false
		}
	}
	return true
}

func (bf *MemoryBloomFilter) hashFun(data []byte, count int) int64 {
	hash := int64(5381)
	for i := 0; i < len(data); i++ {
		hash = hash*33 + int64(data[i]) + int64(count)
	}

	res := hash % (bf.MLength - 1)
	return int64(math.Abs(float64(res)))
}
