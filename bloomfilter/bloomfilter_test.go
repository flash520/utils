/**
 * @Author: koulei
 * @Description:
 * @File: bloomfilter_test.go
 * @Version: 1.0.0
 * @Date: 2022/3/4 18:40
 */

package bloomfilter

import (
	"fmt"
	"testing"
)

func TestBloom(t *testing.T) {
	bf := NewBloomFilter(10000000, 0.0000001)
	fmt.Println(bf)
	bf.Set([]byte("zhuang"))
	bf.Set([]byte("jing"))
	bf.Set([]byte("peng"))

	fmt.Println(bf.Check([]byte("aa")))
	fmt.Println(bf)
}

func BenchmarkNewBloomFilter(b *testing.B) {
	bf := NewBloomFilter(1000000, 0.00000000001)
	bf.Set([]byte("zhuang"))
	bf.Set([]byte("jing"))
	bf.Set([]byte("peng"))
	bf.Set([]byte("zhuang"))
	bf.Set([]byte("jing"))
	bf.Set([]byte("peng"))
	bf.Set([]byte("zhuang"))
	bf.Set([]byte("jing"))
	bf.Set([]byte("peng"))
	bf.Set([]byte("zhuang"))
	bf.Set([]byte("jing"))
	bf.Set([]byte("peng"))
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		bf.Check([]byte("zhang"))
	}
}
