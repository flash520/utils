/**
 * @Author: koulei
 * @Description:
 * @File: main
 * @Version: 1.0.0
 * @Date: 2022/3/2 21:25
 */

package main

import (
	"fmt"
	"io"
	"log"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:8000")
	if err != nil {
		log.Fatalln(err)
	}
	go func() {
		for {
			receive, err := Receive(conn)
			if err != nil {
				if err == io.EOF {
					break
				}
				panic(err)
			}

			select {
			case rs := <-receive:
				fmt.Println(fmt.Sprintf("receive message from server side: %v", rs))
			}
		}
	}()

	_, err = conn.Write(NewByte(4, 5, 6, 10))
	_, err = conn.Write(NewByte(4, 5, 6))
	_, err = conn.Write(NewByte(4, 5, 6))
	if err != nil {
		panic(err)
	}
	select {}
}

func Receive(conn net.Conn) (<-chan []byte, error) {
	var buff = make([]byte, 512, 512)
	var err error
	var n int
	n, err = conn.Read(buff)
	if err != nil {
		return nil, err
	}
	receive := make(chan []byte, 512)
	receive <- buff[:n]
	return receive, nil
}

func NewByte(byts ...byte) []byte {
	var b = make([]byte, 0, 512)
	b = append(b, byts...)
	return b
}
