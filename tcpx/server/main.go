/**
 * @Author: koulei
 * @Description:
 * @File: main
 * @Version: 1.0.0
 * @Date: 2022/3/2 20:37
 */

package main

import (
	"fmt"
	"io"
	"log"
	"net"
)

func main() {
	listener, err := net.Listen("tcp", ":8000")
	if err != nil {
		log.Fatalln(err.Error())
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err.Error())
			break
		}
		go func(conn net.Conn) {
			var oneRead []byte
			var err error
			defer func() {
				_ = conn.Close()
			}()

			for {
				oneRead, err = readOnce(conn)
				if err != nil {
					if err == io.EOF {
						break
					}
					log.Println(err.Error())
					return
				}
				fmt.Println(fmt.Sprintf("client: %v recive : %v", conn.RemoteAddr(), oneRead))

				_, err := conn.Write(NewByte(1, 2, 3, 4))
				if err != nil {
					fmt.Println(err.Error())
					return
				}
			}
		}(conn)
	}
}

func readOnce(reader io.Reader) ([]byte, error) {
	var buff = make([]byte, 512, 512)
	var err error
	var n int

	n, err = reader.Read(buff)
	if err != nil {
		return nil, err
	}

	return buff[:n], nil
}

func NewByte(byts ...byte) []byte {
	var b = make([]byte, 0, 512)
	b = append(b, byts...)
	return b
}
