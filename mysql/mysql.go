/**
 * @Author: koulei
 * @Description: TODO
 * @File:  mysql
 * @Version: 1.0.0
 * @Date: 2021/5/12 18:14
 */

package mysql

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

var (
	db  *gorm.DB
	err error
)

type Mysql struct {
}

// CreateMysql 初始化 MySQL 数据库连接
func CreateMysql(addr, username, password, dbname string, loglevel interface{}, maxConn ...int) *Mysql {
	var logLevel logger.LogLevel
	var DBMaxConn int

	// 默认的数据库连接池大小
	if len(maxConn) > 0 {
		DBMaxConn = maxConn[0]
	} else {
		DBMaxConn = 100
	}
	level := loglevel.(string)
	level = strings.ToUpper(level)
	switch level {
	case "INFO":
		logLevel = logger.Info
	default:
		logLevel = logger.Warn
	}
	var once sync.Once
	once.Do(func() {
		dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&collation=utf8mb4_general_ci&parseTime=True&loc=Local",
			username, password,
			addr,
			dbname)
		db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
			NamingStrategy: schema.NamingStrategy{
				SingularTable: true,
			},
			Logger:      logger.Default.LogMode(logLevel),
			PrepareStmt: true,
			NowFunc: func() time.Time {
				return time.Now().Local()
			},
		})
		if err != nil {
			fmt.Println(err)
		}

		if rawDB, err := db.DB(); err != nil {
			fmt.Println(err.Error())
		} else {
			rawDB.SetMaxIdleConns(10)
			rawDB.SetMaxOpenConns(DBMaxConn)
			rawDB.SetConnMaxLifetime(time.Hour)
		}
	})

	return &Mysql{}
}

func (m *Mysql) GetConn() *gorm.DB {
	return db
}
